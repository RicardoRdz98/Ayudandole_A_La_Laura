<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EquiposSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('equipos')->insert([
            'nom_equipo' => 'Manchester United',
            'jugador' => 'Wayne Rooney'
        ]);

        DB::table('equipos')->insert([
            'nom_equipo' => 'Juventus',
            'jugador' => 'Paulo Dybala'
        ]);

        DB::table('equipos')->insert([
            'nom_equipo' => 'Barcelona',
            'jugador' => 'Leonel Messi'
        ]);
    }
}
